import { Component, OnInit } from '@angular/core';
import {UserService} from '../../Service/user.service';

@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.css']
})
export class UserlistComponent implements OnInit {

  public Users: any[];

  constructor(private userService: UserService) {
  }

  ngOnInit(): void {
    this.userList();
    console.log('hello');
  }

  userList() {
    this.userService.getuserList().subscribe((data: any) => {
      this.Users = data;
    }, error => {
      this.Users = [];

    }, () => {
    });
  }

}
