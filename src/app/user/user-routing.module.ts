import {RouterModule, Routes} from '@angular/router';
import {UserlistComponent} from './userlist/userlist.component';
import {UsercreateComponent} from './usercreate/usercreate.component';
import {NgModule} from '@angular/core';

const routes: Routes = [
  {path: '', component: UserlistComponent, children: [{path: 'list', component: UserlistComponent}]}  ,
  {path: '', component: UsercreateComponent, children: [{path: 'create', component: UsercreateComponent}]}  ,
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
