import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserlistComponent } from './userlist/userlist.component';
import { UsercreateComponent } from './usercreate/usercreate.component';
import {UserRoutingModule} from './user-routing.module';
import {FormsModule} from '@angular/forms';



@NgModule({
  declarations: [UserlistComponent, UsercreateComponent],
  imports: [
    CommonModule,
    UserRoutingModule,
    FormsModule
  ]
})
export class UserModule { }
