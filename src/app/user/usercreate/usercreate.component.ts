import { Component, OnInit } from '@angular/core';
import {User} from '../../User';
import {UserService} from '../../Service/user.service';
import {Router, RouterEvent} from '@angular/router';

@Component({
  selector: 'app-usercreate',
  templateUrl: './usercreate.component.html',
  styleUrls: ['./usercreate.component.css']
})
export class UsercreateComponent implements OnInit {
user: User = new  User();
submitted = false;
  constructor(private userService: UserService, private router: Router) { }

  ngOnInit(): void {
  }
  newUser(): void {
    this.submitted = false;
    this.user = new User();
  }
  save() {
    this.userService.createUser(this.user)
      .subscribe(data => console.log(data), error => console.log(error));
    this.user = new User();
    this.gotoList();
  }


  onSubmit() {
    this.submitted = true;
    this.save();
  }

  gotoList() {
    this.router.navigate(['/create']);
  }

}
