import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {UsercreateComponent} from './user/usercreate/usercreate.component';
import {CommonModule} from '@angular/common';

const routes: Routes = [
  {path: '', redirectTo: 'login', pathMatch: 'full'},
  {path: 'login', loadChildren: () => import('./modules/auth/auth.module').then(m => m.AuthModule)},
  {path: 'members', loadChildren: () => import('./modules/pages/pages.module').then(m => m.PagesModule)},
  {path: 'user', loadChildren: () => import('./user/user.module').then(mod => mod.UserModule)},
  {path: 'create', component: UsercreateComponent },
];

@NgModule({
  imports: [
    CommonModule ,
    RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
