import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
  export class UserService {
  private baseUrl = 'http://localhost:8080/api/users/';
  constructor(private http: HttpClient) { }

  getuserList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }
  // tslint:disable-next-line:ban-types
  createUser(user: Object): Observable<Object> {
    return this.http.post(this.baseUrl + 'adduserss', user);
  }

}
